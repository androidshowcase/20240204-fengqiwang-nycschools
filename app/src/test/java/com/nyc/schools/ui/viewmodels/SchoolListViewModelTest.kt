package com.nyc.schools.ui.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nyc.schools.data.models.School
import com.nyc.schools.data.models.SchoolsResult
import com.nyc.schools.data.repo.SchoolRepo
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolListViewModelTest {
    private val repo: SchoolRepo = mockk()
    private lateinit var viewModel: SchoolListViewModel

    private val dispatcher: TestDispatcher = UnconfinedTestDispatcher()

    private lateinit var viewStates: MutableList<SchoolListViewState>

    private val school = School(
        "1",
        "Fake School 2",
        'B',
        "This is the 2nd fake school",
        1,
        location = "Location 1",
        website = "http://nycschool2.edu"
    )

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        viewModel = SchoolListViewModel(repo)
        viewStates = mutableListOf()

        viewModel.viewState.observeForever {
            viewStates += it
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test getSchools successfully`() = runTest {
        coEvery {
            repo.getSchools()
        } returns flow {
            emit(SchoolsResult.SchoolList(listOf(school)))
        }

        viewModel.getSchools()

        coVerify {
            repo.getSchools()
        }

        Assert.assertEquals(SchoolListViewState.Success(emptyList()), viewStates[0])
        Assert.assertEquals(SchoolListViewState.Loading, viewStates[1])

        delay(500)
        Assert.assertEquals(SchoolListViewState.Success(listOf(school)), viewStates[2])
    }

    @Test
    fun `test getSchools failed`() = runTest {
        coEvery {
            repo.getSchools()
        } returns flow {
            emit(SchoolsResult.Error("ERROR"))
        }
        viewModel.getSchools()

        coVerify {
            repo.getSchools()
        }

        Assert.assertEquals(SchoolListViewState.Success(emptyList()), viewStates[0])
        Assert.assertEquals(SchoolListViewState.Loading, viewStates[1])

        delay(500)
        Assert.assertEquals(SchoolListViewState.Error("ERROR"), viewStates[2])
    }
}