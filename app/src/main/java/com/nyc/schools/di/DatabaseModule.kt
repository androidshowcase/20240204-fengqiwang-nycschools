package com.nyc.schools.di

import android.content.Context
import androidx.room.Room
import com.nyc.schools.data.storage.SatDao
import com.nyc.schools.data.storage.SchoolDB
import com.nyc.schools.data.storage.SchoolDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): SchoolDB {
        return Room.databaseBuilder(
            appContext,
            SchoolDB::class.java,
            "SchoolDatabase"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideSchoolDao(database: SchoolDB): SchoolDao {
        return database.schoolDao
    }

    @Provides
    fun provideSatDao(database: SchoolDB): SatDao {
        return database.satDao
    }
}