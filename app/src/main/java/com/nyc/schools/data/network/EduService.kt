package com.nyc.schools.data.network

import com.nyc.schools.data.models.SatInfo
import com.nyc.schools.data.models.School
import retrofit2.Response
import retrofit2.http.GET

interface EduService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<School>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATResults(): Response<List<SatInfo>>
}

const val BASE_URL = "https://data.cityofnewyork.us/"