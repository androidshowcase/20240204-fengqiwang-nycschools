package com.nyc.schools.data.repo

import com.nyc.schools.data.models.SatInfo
import com.nyc.schools.data.models.School
import com.nyc.schools.data.models.SchoolsResult
import com.nyc.schools.data.network.EduService
import com.nyc.schools.data.storage.SatDao
import com.nyc.schools.data.storage.SchoolDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolRepo @Inject constructor(
    private val eduService: EduService,
    private val schoolDao: SchoolDao,
    private val satDao: SatDao
) {
    suspend fun getSchools(): Flow<SchoolsResult> = flow {
        emit(
            try {
                val response = eduService.getSchools()
                if (response.isSuccessful) {
                    val schools = response.body() ?: emptyList()
                    schoolDao.saveAll(schools)
                    SchoolsResult.SchoolList(schools)
                } else SchoolsResult.Error(response.errorBody()?.toString().orEmpty())
            } catch (e: Exception) {
                schoolDao.saveAll(FAKE_SCHOOLS)
                SchoolsResult.Error(e.message.orEmpty())
            }
        )

        emit(SchoolsResult.SchoolList(schoolDao.allSchools()))
    }

    suspend fun getSatInfo(dbn: String): Flow<SatInfo> = flow {
        try {
            val response = eduService.getSATResults()
            if (response.isSuccessful) {
                response.body()?.let {
                    satDao.saveAll(it)
                }
            }
        } catch (e: Exception) {
            println(e.message)
            satDao.saveAll(listOf(FAKE_SAT))
        }

        emit(satDao.fromDbn(dbn))
    }

    private val FAKE_SCHOOLS = listOf(
        School(
            "0",
            "Fake School 1",
            'A',
            "This is the 1st fake school",
            1,
            location = "Location 0",
            website = "http://nycschool1.edu"
        ),
        School(
            "1",
            "Fake School 2",
            'B',
            "This is the 2nd fake school",
            1,
            location = "Location 1",
            website = "http://nycschool2.edu"
        )
    )
    private val FAKE_SAT = SatInfo(
        "1",
        "Fake School 2",
        "100",
        "460",
        "495",
        "420"
    )
}