package com.nyc.schools.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class School(
    @PrimaryKey
    val dbn: String,
    val school_name: String,
    val boro: Char,
    val overview_paragraph: String,
    val school_10th_seats: Int,
    val academicopportunities1: String? = null,
    val ell_programs: String? = null,
    val language_classes: String? = null,
    val advancedplacement_courses: String? = null,
    val location: String? = null,
    val website: String? = null
)