package com.nyc.schools.data.models

sealed interface SchoolsResult {
    data class SchoolList(val schools: List<School>) : SchoolsResult

    data class Error(val errorMessage: String) : SchoolsResult
}