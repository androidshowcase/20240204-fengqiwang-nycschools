package com.nyc.schools.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class SatInfo(
    @PrimaryKey
    val dbn: String,
    @SerializedName("school_name") val schoolName: String? = null,
    @SerializedName("num_of_sat_test_takers") val takers: String? = null,
    @SerializedName("sat_critical_reading_avg_score") val reading: String? = null,
    @SerializedName("sat_math_avg_score") val math: String? = null,
    @SerializedName("sat_writing_avg_score") val writing: String? = null
)
