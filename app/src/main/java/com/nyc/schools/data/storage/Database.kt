package com.nyc.schools.data.storage

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.RoomDatabase
import com.nyc.schools.data.models.SatInfo
import com.nyc.schools.data.models.School

@Dao
interface SchoolDao {
    @Query("select * from School")
    suspend fun allSchools(): List<School>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAll(records: List<School>)
}

@Dao
interface SatDao {
    @Query("select * from SatInfo where dbn == :dbn")
    suspend fun fromDbn(dbn: String): SatInfo

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAll(records: List<SatInfo>)
}


@Database(entities = [School::class, SatInfo::class], version = 1, exportSchema = false)
abstract class SchoolDB: RoomDatabase() {
    abstract val schoolDao: SchoolDao

    abstract val satDao: SatDao
}