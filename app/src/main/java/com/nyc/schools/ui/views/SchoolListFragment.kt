package com.nyc.schools.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nyc.schools.R
import com.nyc.schools.data.models.School
import com.nyc.schools.databinding.FragmentListBinding
import com.nyc.schools.ui.adapters.SchoolAdapter
import com.nyc.schools.ui.viewmodels.SchoolListViewModel
import com.nyc.schools.ui.viewmodels.SchoolListViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SchoolListFragment : Fragment(), SchoolAdapter.SchoolClickListener {
    private val viewModel: SchoolListViewModel by viewModels()

    private val schoolAdapter by lazy { SchoolAdapter(this) }

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater)

        viewModel.viewState.observe(viewLifecycleOwner) {
            binding.progress = it is SchoolListViewState.Loading

            when (it) {
                is SchoolListViewState.Success -> {
                    schoolAdapter.updateSchools(it.data)
                }

                is SchoolListViewState.Error -> {
                    Snackbar.make(
                        binding.schoolList,
                        it.info,
                        BaseTransientBottomBar.LENGTH_LONG
                    ).show()
                }

                else -> Unit
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                binding.searchView.query.takeIf { it.isNotBlank() }?.let {
                    viewModel.getSchools()
                } ?: run { viewModel.getSchools() }
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.schoolList.adapter = schoolAdapter
    }

    override fun onClick(view: View, school: School) {
        findNavController().navigate(
            R.id.action_listFragment_to_satFragment,
            bundleOf("dbn" to school.dbn)
        )
    }
}