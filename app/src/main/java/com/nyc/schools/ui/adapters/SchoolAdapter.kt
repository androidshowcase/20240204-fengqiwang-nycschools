package com.nyc.schools.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.nyc.schools.data.models.School
import com.nyc.schools.databinding.ItemSchoolBinding
import com.nyc.schools.BR


class SchoolAdapter(private val schoolClickListener: SchoolClickListener) :
    RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>() {

    private val schools = mutableListOf<School>()

    override fun getItemCount(): Int = schools.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ItemSchoolBinding =
            ItemSchoolBinding.inflate(layoutInflater, parent, false)
        return SchoolViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(schools[position], schoolClickListener)

    fun updateSchools(schools: List<School>) {
        this.schools.clear()
        this.schools.addAll(schools)
        notifyDataSetChanged()
    }

    class SchoolViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School, schoolClickListener: SchoolClickListener) {
            with(binding) {
                setVariable(BR.school, school)
                setVariable(BR.clickHandler, schoolClickListener)
                executePendingBindings()
            }
        }
    }

    interface SchoolClickListener {
        fun onClick(view: View, school: School)
    }
}
