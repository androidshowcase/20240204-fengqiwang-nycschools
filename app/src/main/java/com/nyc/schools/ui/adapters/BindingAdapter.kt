package com.nyc.schools.ui.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.nyc.schools.R
import com.squareup.picasso.Picasso

private const val BASE_IMAGE_URL = ""

@BindingAdapter("url")
fun loadSchoolPoster(view: ImageView, url: String?) {
    url.takeUnless { it.isNullOrEmpty() }?.let {
        Picasso.get().load("$BASE_IMAGE_URL$it")
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(view)
    }
}


