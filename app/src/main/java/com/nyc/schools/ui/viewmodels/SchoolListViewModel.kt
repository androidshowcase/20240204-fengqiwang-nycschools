package com.nyc.schools.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.data.repo.SchoolRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel  @Inject constructor(private val repo: SchoolRepo): ViewModel() {
    private val _viewState = MutableLiveData<SchoolListViewState>().apply {
        value = SchoolListViewState.Success(emptyList())
    }

    val viewState: LiveData<SchoolListViewState>
        get() = _viewState

    fun getSchools() {
        _viewState.value = SchoolListViewState.Loading

        viewModelScope.launch(Dispatchers.IO) {
            repo.getSchools().collect {
                withContext(Dispatchers.Main) {
                    _viewState.value = it.toViewState()
                }
            }
        }
    }
}