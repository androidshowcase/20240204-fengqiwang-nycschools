package com.nyc.schools.ui.viewmodels

import com.nyc.schools.data.models.School
import com.nyc.schools.data.models.SchoolsResult

sealed interface SchoolListViewState {
    data object Loading : SchoolListViewState

    data class Error(val info: String) : SchoolListViewState

    data class Success(val data: List<School>) : SchoolListViewState
}

fun SchoolsResult.toViewState(): SchoolListViewState =
    when (this) {
        is SchoolsResult.SchoolList -> SchoolListViewState.Success(this.schools)
        is SchoolsResult.Error -> SchoolListViewState.Error(errorMessage)
    }