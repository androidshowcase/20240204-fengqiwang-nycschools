# NYC High Schools

 - Display a list of NYC High Schools
 - Selecting a school should show SAT information about the school

## IDE: 
Android Studio Hedgehog
compileSdk = 34
Kotlin 1.9

## Open-Source Libraries used:
- Retrofit
- Picasso
- Mockk

## Jetpack frameworks:
- Hilt/Dagger
- Navigation


## TODOes:

    - Functionality:
        - UI refinement
        - Consume more School Details (e.g, neighborhood)

    - Non-Functional:
        -  Pagination
        -  Verify stability
		-  Improve Performance 
		-  more Unit Test and Automation Test
		-  Modularization (app / feature modules)